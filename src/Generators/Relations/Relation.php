<?php


namespace Ratiborro\LaravelRelations\Generators\Relations;


interface Relation
{
    public function model(): string;

    public function name(): string;

    public function type(): string;

    public function foreignKey(): string;

    public function ownerKey(): string;

    public function keys(): string;

    public function stringifiedKeys(): string;
}
