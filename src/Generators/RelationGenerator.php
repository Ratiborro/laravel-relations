<?php


namespace Ratiborro\LaravelRelations\Generators;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Ratiborro\LaravelRelations\Helpers\Database;
use Ratiborro\LaravelRelations\Helpers\File;
use Ratiborro\LaravelRelations\Reflectors\ModelReflector;

class RelationGenerator
{
    protected $tws = '_';
    protected $db;
    protected $table;
    protected $originalModel;
    protected $model;
    protected $namespace;
    protected $tables = [];
    protected $models = [];
    protected $columns = [];
    //protected $uses = [];
    protected $relations = [];
    protected $relationStrings = [];
    protected $foreignKeys = [];
    protected $excludedRelations = [];

    public function __construct()
    {
        $this->db = Database::make();
        $this->models = File::getModels();
        $this->tables = $this->db->getTables();
    }

    public function setModels(array $models): self
    {
        $this->models = $models;
        return $this;
    }

    public function setTables(array $tables): self
    {
        $this->tables = $tables;
        return $this;
    }

    public function setModelsNamespace(string $namespace): self
    {
        $this->namespace = Str::finish($namespace, '\\');
        return $this;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;
        if (!isset($this->originalModel)) {
            $this->originalModel = $this->model;
        }
        return $this;
    }

    public function setTable(string $table): self
    {
        $this->table = $table;
        $this->model = $this->models[$table] ?? null;
        if (!isset($this->originalModel)) {
            $this->originalModel = $this->model;
        }
        $this->db->setTable($this->table);
        $this->columns = $this->db->getTableColumns();
        $this->foreignKeys = $this->db->getForeignKeys();
        return $this;
    }

    public function excludeRelations(array $relations): self
    {
        $this->excludedRelations = $relations;
        return $this;
    }

    public function buildRelations(): array
    {
        $this->generateBelongsToRelations();
        $this->generateHasOneManyRelations();
        $this->generateBelongsToManyRelations();

        $stubs = $this->getStubs();

        $stubPaths = [];
        foreach ($stubs as $stub) {
            $stubPaths[$stub->getBasename('.stub')] = $stub->getRealPath();
        }

        $this->relations = array_filter($this->relations, function ($relation) {
            return !in_array($relation['name'], $this->excludedRelations);
        });

        usort($this->relations, function ($a, $b) {
            return $a['name'] <=> $b['name'];
        });

        $this->relationStrings = [];
        $originalModelReflector = (new ModelReflector())->setModel($this->originalModel);
        foreach ($this->relations as $relation) {
            /*if (!class_exists($relation['model'], false)) {
                $this->uses[] = $relation['model'];
            }*/
            $modelReflector = (new ModelReflector())->setModel($relation['model']);
            if ($modelReflector->getNamespace() === $originalModelReflector->getNamespace()) {
                $relation['model'] = $modelReflector->getBasename() . '::class';
            } else {
                $relation['model'] = '\\' . $relation['model'] . '::class';
            }
            $path = $stubPaths[lcfirst($relation['type'])] ?? $stubPaths[config('laravel-relations.stubs.default_type')];
            $stub = $this->readStub($path);
            $this->replaceName($stub, $relation['name'])
                ->replaceParams($stub, array_filter($relation, function ($value, $key) {
                    return !in_array($key, ['name', 'type']);
                }, ARRAY_FILTER_USE_BOTH));

            $this->relationStrings[] = $stub;
        }
        return $this->relations;
    }

    public function getRelationsString(): string
    {
        return $this->stringifyRelations();
    }

    /*public function getUsesString(): string
    {
        return $this->stringifyUses();
    }*/

    protected function stringifyRelations(): string
    {
        $content = '';
        foreach ($this->relationStrings as $relation) {
            $content .= PHP_EOL . $relation;
        }
        return $content;
    }

    /*protected function stringifyUses(): string
    {
        $content = '';
        foreach ($this->uses as $use) {
            $content .= "use \\$use;" . PHP_EOL;
        }
        if ($content) {
            $content .= PHP_EOL;
        }
        return $content;
    }*/

    protected function getForeignKeyEndings(): array
    {
        return ['_id'];
    }

    protected function getRelationName(string $column): ?string
    {
        foreach ($this->getForeignKeyEndings() as $ending) {
            $name = Str::before($column, $ending);
            if ($name !== $column) {
                return $name;
            }
        }
        return null;
    }

    protected function generateSingularRelationName(string $column): ?string
    {
        return Str::camel($this->getRelationName($column)) ?: null;
    }

    protected function generatePluralRelationName(string $column): ?string
    {
        return Str::camel(Str::plural($this->getRelationName($column))) ?: null;
    }

    protected function generateBelongsToRelations()
    {
        $columns = $this->db->getTableColumns();
        $model = $this->getModelName();
        if (empty($model)) return;

        foreach ($columns as $column) {
            if ($this->isForeignKeyCandidate($column)) {

                $relation = [
                    'name' => $this->generateSingularRelationName($column),
                    'type' => 'belongsTo',
                    'model' => null,
                    'ownerKey' => $column,
                    'foreignKey' => null,
                ];

                $foreignKey = $this->getForeignKey($column);
                if ($foreignKey) {
                    $relationModel = $this->models[$foreignKey->getForeignTableName()] ?? null;
                    $relation['model'] = $relationModel ? $this->namespace . $relationModel : null;
                    $relation['foreignKey'] = Arr::first($foreignKey->getForeignColumns());
                }

                if (!isset($relation['model'])) {
                    $relationModel = $this->getRelationModelName($column);
                    if (class_exists($relationModel)) {
                        $relation['model'] = $relationModel;
                        $relation['foreignKey'] = $relationModel::make()->getKeyName();
                    } else {
                        continue;
                    }
                }

                $isStandardNaming = Str::endsWith($column, '_id') && Str::snake(basename($relation['name'])) . '_id' === $column;
                $withOptionalParams = config('laravel-relations.relations.with_optional_params', false);
                if ($isStandardNaming && !$withOptionalParams) {
                    unset($relation['ownerKey'], $relation['foreignKey']);
                }

                $this->relations[] = $relation;
            }
        }
    }

    protected function generateHasOneManyRelations()
    {
        $columnStartsWith = Str::snake($this->model);
        foreach ($this->tables as $table) {
            $this->db->setTable($table);
            $model = $this->getModelName($table);
            if (empty($model)) continue;
            $columns = $this->db->getTableColumns();
            $foreignKeys = $this->db->getForeignKeys();

            foreach ($columns as $column) {
                if (Str::startsWith($column, $columnStartsWith) && $this->isForeignKeyCandidate($column)) {
                    $relation = [
                        'name' => Str::camel(Str::plural($table)),
                        'type' => !empty($this->db->getUniqueIndex($column))
                            ? 'hasOne'
                            : 'hasMany',
                        'model' => $model,
                        'foreignKey' => $column,
                        'localKey' => null,
                    ];

                    $foreignKey = $this->getForeignKey($column, $foreignKeys);
                    if ($foreignKey) {
                        $relationModel = $this->models[$foreignKey->getForeignTableName()] ?? null;
                        $relation['model'] = $relationModel ? $this->namespace . $relationModel : null;
                        $relation['localKey'] = Arr::first($foreignKey->getForeignColumns());
                    }

                    if (!isset($relation['localKey'])) {
                        $relationModel = $this->getRelationModelName($column);
                        if (class_exists($relationModel)) {
                            $relation['localKey'] = $relationModel::make()->getKeyName();
                        } else {
                            continue;
                        }
                    }

                    $isStandardNaming = Str::endsWith($column, '_id') && Str::snake(basename($column)) === $column;
                    $withOptionalParams = config('laravel-relations.relations.with_optional_params', false);
                    if ($isStandardNaming && !$withOptionalParams) {
                        unset($relation['localKey'], $relation['foreignKey']);
                    }

                    $this->relations[] = $relation;
                }
            }
        }
    }

    protected function getRelationModelName(string $column)
    {
        return $this->namespace . Str::studly(Str::before($column, '_id'));
    }

    protected function getForeignKey(string $column, array $foreignKeys = null)
    {
        return Arr::first($foreignKeys ?? $this->foreignKeys, function ($foreignKey) use ($column) {
            $localColumns = $foreignKey->getLocalColumns();
            $localColumn = Arr::first($localColumns);
            return count($localColumns) === 1
                && $localColumn === $column;
        });
    }

    protected function isForeignKeyCandidate(string $column): bool
    {
        return Str::endsWith($column, $this->getForeignKeyEndings());
    }

    protected function getModelName(string $table = null)
    {
        $name = $this->models[$table ?? $this->table] ?? null;
        if ($name) {
            $name = $this->namespace . $name;
        }
        return $name;
    }

    protected function generateModelName(string $column): ?string
    {
        $endings = $this->getForeignKeyEndings();
        foreach ($endings as $ending) {
            $name = Str::ucfirst(Str::studly(Str::before($column, $ending)));
            if ($name) {
                return $this->namespace . $name;
            }
        }
        return null;
    }

    protected function getPivotTables()
    {
        $singular = Str::singular($this->table);
        return array_filter($this->tables, function ($table) use ($singular) {
            if (!Str::contains($table, $singular)) {
                return false;
            }
            $relationTablePrefix = $singular . $this->tws;
            if (Str::startsWith($table, $relationTablePrefix)) {
                $after = Str::after($relationTablePrefix, $table);
                if ($after && $after === Str::singular($after)) {
                    return true;
                }
            }
            $relationTablePostfix = $this->tws . $singular;
            if (Str::endsWith($table, $relationTablePostfix)) {
                $before = Str::before($relationTablePostfix, $table);
                if ($before && $before === Str::singular($before)) {
                    return true;
                }
            }
            return false;
        });
    }

    protected function generateBelongsToManyRelations()
    {
        $pivotTables = $this->getPivotTables();
        $singular = Str::lower(Str::singular($this->model));
        $relation1 = Str::plural($singular);
        $qualifiedParentModelName = $this->namespace . $this->model;

        foreach ($pivotTables as $table) {
            $relation2 = Str::plural(
                Str::startsWith($table, $singular . $this->tws)
                    ? Str::after($table, $singular . $this->tws)
                    : Str::before($table, $this->tws . $singular)
            );
            if (!in_array($relation1, $this->tables)
                || !in_array($relation2, $this->tables)) {
                continue;
            }
            $columns = array_flip($this->db->setTable($table)->getTableColumns());
            $singular2 = Str::singular($relation2);
            foreach ($this->getForeignKeyEndings() as $ending) {
                $column1 = $singular . $ending;
                $column2 = $singular2 . $ending;
                $hasRelationColumns = isset($columns[$column1], $columns[$column2]);
                if (!$hasRelationColumns) {
                    continue;
                }
                $qualifiedRelatedModelName = $this->namespace . $this->models[$relation2];

                $relation = [
                    'name' => Str::camel($relation2),
                    'type' => 'belongsToMany',
                    'model' => $qualifiedRelatedModelName,
                ];

                $withOptionalParams = config('laravel-relations.relations.with_optional_params', false);
                if ($withOptionalParams) {
                    $relation['pivotTable'] = $table;
                }
                if ($column1 !== $singular . '_id' || $withOptionalParams) {
                    $relation['foreignPivotKey'] = $column1;
                }
                if ($column2 !== $singular2 . '_id' || $withOptionalParams) {
                    $relation['relatedPivotKey'] = $column2;
                }
                if ($withOptionalParams) {
                    $relation['parentKey'] = $qualifiedParentModelName::make()->getKeyName();
                    $relation['relatedKey'] = $qualifiedRelatedModelName::make()->getKeyName();
                }

                $this->relations[] = $relation;
            }

        }
    }

    protected function getStubs(): array
    {
        $path = config('laravel-relations.stubs.path', base_path('stubs/laravel-relations'));
        if (!is_dir($path)) {
            $path = dirname(__DIR__) . '/Console/Commands/stubs';
        }
        return File::files($path);
    }

    protected function readStub(string $path): string
    {
        return file_get_contents($path);
    }

    protected function replaceName(&$stub, string $name): self
    {
        $stub = str_replace('{{$name}}', $name, $stub);
        return $this;
    }

    protected function replaceParams(&$stub, array $params): self
    {
        $model = array_shift($params);
        $params = implode('\', \'', $params);
        $params = $params ? "$model, '$params'" : $model;
        $stub = str_replace('{{$params}}', $params, $stub);
        return $this;
    }
}
