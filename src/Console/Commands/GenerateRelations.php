<?php

namespace Ratiborro\LaravelRelations\Console\Commands;

use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Str;
use Ratiborro\LaravelRelations\Generators\RelationGenerator;
use Ratiborro\LaravelRelations\Helpers\DB;
use Ratiborro\LaravelRelations\Helpers\File;
use Ratiborro\LaravelRelations\Reflectors\ModelReflector;

class GenerateRelations extends Command
{
    protected $signature = 'make:relations {model}';
    protected $description = 'Generating model relations in existing model files by database columns';

    protected $startLine = 0;
    //protected $usesLine = 0;
    protected $classEndLine = 0;

    protected $relations;
    protected $path;
    protected $tables;
    protected $columns;
    protected $model;

    protected $relationGenerator;
    protected $modelReflector;

    public function __construct(ModelReflector $modelReflector, RelationGenerator $relationGenerator)
    {
        parent::__construct();
        $this->modelReflector = $modelReflector;
        $this->relationGenerator = $relationGenerator;
    }

    public function handle()
    {
        /* TODO Create dictionary for changing relation names */

        $this->model = $this->getQualifiedModelName($this->argument('model'));


        if (!$this->modelReflector->isModelExists($this->model)) {
            $this->error("The model {$this->model} does not exist. Make sure the model namespace is correct.");
            return 1;
        }

        $this->modelReflector->setModel($this->model);

        try {
            $modelRelations = $this->modelReflector->getModelRelations();
            $existingRelationNames = array_column($modelRelations, 'name');
        } catch (Exception $exception) {
            $this->error('Fails to get model relations. Make sure the model is the correct PHP class');
            return 2;
        }
        // TODO Check Relation::morphMap

        $this->calculateStartLine($modelRelations);
        //$this->calculateUsesLine();


        try {
            $models = File::getModels();
        } catch (Exception $exception) {
            $this->error('Fails to get models');
            return 4;
        }

        try {
            $relations = $this->relationGenerator
                ->setModel($this->model)
                ->setTable($this->modelReflector->getModelTable())
                ->setModelsNamespace($this->getModelsNamespace())
                ->excludeRelations($existingRelationNames)
                ->setModels($models)
                ->buildRelations();
        } catch (Exception $exception) {
            $this->error('Fails to build relations');
            return 4;
        }

        if (!count($relations)) {
            $this->info('All relations have already been generated!');
            return 0;
        }

        try {
            $isGenerated = $this->modelReflector->addContent(
                $this->startLine,
                $this->relationGenerator->getRelationsString()
            );
            /*$uses = $this->relationGenerator->getUsesString();
            if ($uses) {
                $this->modelReflector->addContent(
                    $this->usesLine,
                    $uses
                );
            }*/
        } catch (Exception $exception) {
            $this->error('Fails to add relations to model');
            return 5;
        }

        $isGenerated
            ? $this->info("Relations successfully integrated to $this->model model!")
            : $this->error("Error on integrating relations to $this->model model!");

        return 0;
    }

    protected function getQualifiedModelName(string $name): ?string
    {
        $namespace = $this->getModelsNamespace();
        $name = trim($name, '\\/');
        $name = str_replace('/', '\\', $name);
        return $namespace ? "$namespace\\$name" : $name;
    }

    protected function getModelsNamespace(): string
    {
        return config('laravel-relations.models.base_namespace', 'App\\Models');
    }

    protected function calculateStartLine(array $modelRelations): int
    {
        $modelRelations = array_filter($modelRelations, function ($r) {
            return $r->class === $this->model;
        });
        if (empty($modelRelations)) {
            $this->startLine = $this->modelReflector->getClassEndLine();
        } else {
            foreach ($modelRelations as $relation) {
                $line = $relation->getEndLine();
                if ($line > $this->startLine) {
                    $this->startLine = $line;
                }
            }
        }
        return $this->startLine;
    }

    /*protected function calculateUsesLine()
    {
        $lines = file($this->modelReflector->getPath());
        foreach ($lines as $number => $line) {
            $line = trim($line);
            if (Str::startsWith($line, 'use')) {
                $this->usesLine = $number;
                break;
            } elseif (Str::startsWith($line, 'class')) {
                $this->usesLine = $this->modelReflector->getClassStartLine();
                break;
            }
        }
    }*/
}
