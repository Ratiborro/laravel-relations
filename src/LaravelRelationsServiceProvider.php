<?php

namespace Ratiborro\LaravelRelations;

use Illuminate\Support\ServiceProvider;
use Ratiborro\LaravelRelations\Console\Commands\GenerateRelations;
use Ratiborro\LaravelRelations\Generators\RelationGenerator;
use Ratiborro\LaravelRelations\Reflectors\ModelReflector;

class LaravelRelationsServiceProvider extends ServiceProvider
{
    const PACKAGE_NAME = 'laravel-relations';

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path(static::PACKAGE_NAME . '.php'),
            ], static::PACKAGE_NAME . '-config');

            $this->publishes([
                __DIR__ . '/Console/Commands/stubs' => base_path('stubs/' . static::PACKAGE_NAME),
            ], static::PACKAGE_NAME . '-stubs');

        }
    }

    public function register()
    {
        $this->app->singleton('command.laravel-relations', function () {
            return new GenerateRelations(
                new ModelReflector(),
                new RelationGenerator()
            );
        });
        $this->commands(['command.laravel-relations']);

        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', static::PACKAGE_NAME);
    }

    public function provides()
    {
        return ['command.laravel-relations'];
    }

}
