<?php

namespace Ratiborro\LaravelRelations\Helpers;

use Illuminate\Support\Str;

class File extends \Illuminate\Support\Facades\File
{
    public static function getModels(string $path = null): array
    {
        $path = $path ?: config('laravel-relations.models.path');

        $models = config('laravel-relations.models.is_recursive_search', true)
            ? static::allFiles($path)
            : static::files($path);

        $namespace = config('laravel-relations.models.base_namespace', 'App\\Models');
        $namespace = str_replace('/', '\\', Str::finish($namespace, '\\'));
        $abstractModelClass = config(
            'laravel-relations.models.is_subclass_of',
            \Illuminate\Database\Eloquent\Model::class
        );

        return collect($models)
            ->mapWithKeys(function ($item) use ($namespace, $abstractModelClass) {
                $filename = Str::before(
                    $item->getRelativePathname(),
                    '.' . pathinfo($item->getPathname(), PATHINFO_EXTENSION)
                );
                if (!is_subclass_of($namespace . $filename, $abstractModelClass)) {
                    return [null];
                }
                $model = $namespace . $filename;
                $table = $model::make()->getTable();
                return [$table => $filename];
            })
            ->filter()
            ->toArray();
    }
}
