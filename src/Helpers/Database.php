<?php

namespace Ratiborro\LaravelRelations\Helpers;

use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Index;
use Doctrine\DBAL\Schema\Table;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

class Database
{
    protected $schemaManager;

    protected $table;
    protected $tables;
    protected $indexes;
    protected $options;
    protected $tableDetails;
    protected $foreignKeys;
    protected $uniqueConstraints;

    public function __construct()
    {
        $this->setSchemaManager($this->getSchemaManager());
    }

    protected function getSchemaManager(): AbstractSchemaManager
    {
        return $this->schemaManager ?? Schema::getConnection()->getDoctrineSchemaManager();
    }

    public static function make()
    {
        return new static;
    }

    public function getTables()
    {
        return $this->tables ?: $this->tables = $this->schemaManager->listTableNames();
    }

    public function setSchemaManager(AbstractSchemaManager $manager): self
    {
        $this->schemaManager = $manager;
        return $this;
    }

    public function setTable(string $table): self
    {
        $this->table = $table;
        $this->getTableDetails();
        return $this;
    }

    public function getTableColumns(): array
    {
        return Schema::getColumnListing($this->table);
    }

    public function getTableDetails(): Table
    {
        return $this->tableDetails = $this->tableDetails ?: $this->schemaManager->listTableDetails($this->table);
    }

    public function getUniqueConstraints(): array
    {
        return $this->uniqueConstraints = $this->uniqueConstraints ?: $this->tableDetails->getUniqueConstraints();
    }

    public function getIndexes(): array
    {
        return $this->indexes = $this->indexes ?: $this->tableDetails->getIndexes();
    }

    public function getUniqueIndexes(): array
    {
        return array_filter(
            $this->getIndexes(),
            static function ($index) {
                return $index->isUnique();
            }
        );
    }

    public function getUniqueIndex(string $column): ?Index
    {
        return Arr::first(array_filter(
            $this->getUniqueIndexes(),
            static function ($index) use ($column) {
                $columns = $index->getColumns();
                return count($columns) === 1 && isset($columns[$column]);
            }
        ));
    }

    public function getForeignKeys(): array
    {
        return $this->foreignKeys = $this->foreignKeys ?: $this->tableDetails->getForeignKeys();
    }

    public function getOptions(): array
    {
        return $this->options = $this->options ?: $this->tableDetails->getOptions();
    }


}
