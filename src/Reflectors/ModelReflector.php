<?php

namespace Ratiborro\LaravelRelations\Reflectors;

use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;

class ModelReflector
{
    protected $model;
    protected $instance;
    protected $table;
    protected $reflector;
    protected $path;
    protected $classStartLine;
    protected $classEndLine;
    protected $methods;
    protected $relations;

    protected const RELATIONS = [
        'hasOne',
        'hasOneThrough',
        'hasMany',
        'hasManyThrough',
        'belongsTo',
        'belongsToMany',
        'morphTo',
        'morphOne',
        'morphMany',
        'morphedByMany',
    ];

    public function setModel(string $model): self
    {
        $this->model = $model;
        $this->reflector = new ReflectionClass($this->model);
        $this->instance = $this->model::make();
        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getBasename(): ?string
    {
        return $this->reflector->getShortName();
    }

    public function getNamespace(): ?string
    {
        return $this->reflector->getNamespaceName();
    }

    public function getClassStartLine(): ?int
    {
        return $this->classStartLine ?: ($this->classStartLine = $this->reflector->geStartLine());
    }

    public function getClassEndLine(): ?int
    {
        return $this->classEndLine ?: ($this->classStartLine = $this->reflector->getEndLine());
    }

    public function isModelExists(string $model): bool
    {
        return class_exists($model ?? $this->model);
    }

    public function getModelMethods($filter = ReflectionMethod::IS_PUBLIC): array
    {
        $this->path = $this->reflector->getFileName();
        $this->classEndLine = $this->reflector->getEndLine() - 1;
        $this->classStartLine = $this->reflector->getStartLine();
        return $this->methods = $this->reflector->getMethods($filter);
    }

    public function getModelRelations(): array
    {
        if (!isset($this->methods)) {
            $this->methods = $this->getModelMethods();
        }

        $this->relations = [];
        $modelsNamespace = $this->getModelsNamespace();
        $reflectors = [];
        foreach ($this->methods as $method) {
            if (Str::startsWith($method->class, $modelsNamespace) && empty($method->getParameters())) {
                $reflector = $reflectors[$method->class] ?? ($reflectors[$method->class] = new ReflectionClass($method->class));
                $lines = array_slice(
                    file($reflector->getFileName()),
                    $method->getStartLine(),
                    $method->getEndLine() - $method->getStartLine()
                );
                foreach ($lines as $line) {
                    $line = trim($line);
                    if (Str::startsWith($line, array_map(function ($r) {
                        return 'return $this->' . $r;
                    }, self::RELATIONS))) {
                        $this->relations[] = $method;
                        break;
                    }
                }
            }
        }
        return $this->relations;
    }

    public function getModelTable(): string
    {
        return $table ?? $this->table = $this->instance->getTable();
    }

    public function getModelsNamespace(): string
    {
        return config('laravel-relations.models.base_namespace', 'App\\Models');
    }

    public function addContent(int $startLine, string $content): bool
    {
        $lines = file($this->path);
        $startLineContent = $lines[$startLine] ?? null;
        $lines[$startLine] = $content . $startLineContent;
        return (bool)file_put_contents($this->path, implode('', $lines));
    }
}
