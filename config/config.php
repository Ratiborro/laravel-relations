<?php
return [
    'models' => [
        'base_namespace' => 'App\\Models',
        'path' => app_path('Models'), // Searching models in this path
        'is_recursive_search' => true, // Searching in subdirectories
        'is_subclass_of' => \Illuminate\Database\Eloquent\Model::class // Model is a subclass of the specified class
    ],
    'stubs' => [
        'path' => base_path('stubs/laravel-relations'),
        'default_type' => 'belongsTo'
    ],
    'relations' => [
        'with_optional_params' => false
    ]
];
